@extends('website.layouts.app')

@section('content')

    <div class="login-page">
        <div class="container">
            <div class="col-md-8 col-md-offset-2 pull-left col-sm-12 col-xs-12">

                <h3>تسجيل الدخول</h3>

                <div class="login-form">
                    @include('message')
                    <form action="{{route('login')}}" method="post">
                        @csrf
                        <div class="form-g">
                            <input type="text" name="username" placeholder="اسم المستخدم  - رقم الجوال">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('username') }}
                                   </strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-g">
                            <input type="password" name="password" placeholder="الرقم السري">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password') }}
                                   </strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-g">
                            <input type="checkbox">
                            <span>تذكرني</span>
                        </div>

                        <div class="form-g">
                            <button type="submit">دخول</button>
                        </div>
                    </form>

                    <div class="register-btn">
                        <a href="/register">تسجيل جديد</a>
                    </div>
                    <!--<div class="form-group row mb-0">-->
                    <!--    <div class="col-md-12">-->
                    <!--        <div class="social-login">-->
                    <!--            <ul>-->
                    <!--                <li><a href="{{ url('/redirect/facebook') }}" class="facebook"><i-->
                    <!--                            class="fa fa-facebook"></i></a></li>-->
                    <!--                <li><a href="{{ url('/redirect/google') }}" class="google"><i-->
                    <!--                            class="fa fa-google"></i></a></li>-->
                    <!--            </ul>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                </div>
            </div>
        </div>
    </div>



@endsection
