@extends("website.layouts.app")
@section('content')
    <div class="login-page">
        <div class="container">
            <div class="col-md-6">

                <h3>تسجيل جديد</h3>

                <div class="login-form">
                    <form action="{{route('register')}}" method="post">
                        @csrf
                        <div class="form-g">
                            <input type="text" name="phone" value="{{old('phone')}}" placeholder="ادخل رقم الجوال">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('phone') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="username" value="{{old('username')}}" placeholder="ادخل اسم المستخدم">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('username') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="email" name="email" value="{{old('email')}}" placeholder="ادخل البريد الالكترونى">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('email') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password"  placeholder="ادخل كلمة المرور">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password_confirmation"  placeholder="ادخل تأكيد كلمة المرور">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password_confirmation') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <select name="country_id" id="country_id" class="form-control" placeholder="الدولة">
                                <option value=""> اختر الدولة</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}"> {{$country->name_ar}}</option>

                                @endforeach

                            </select>
                            @if ($errors->has('country_id'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('country_id') }}
                                   </strong>
                                </span>
                            @endif
                        </div><br>
                        <div class="form-g">
                            <select name="city_id" id="city_id" class="form-control">
                                
                                <option value=""> اختر المدينة</option>
                            </select>
                            @if ($errors->has('city_id'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('city_id') }}
                                   </strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-g">
                            <button type="submit">سجل الان</button>
                        </div>
                    </form>
                </div>
            </div>


            <div class="col-md-6">

                <h3>التسجيل بالموقع</h3>

                <div class="login-form register-text">
                    للتسجيل بالموقع، يجب اولا الموافقة على اتفاقية الاستخدام:

                    إن اتفاقية الاستخدام هذه وخصوصية الاستخدام ، والشروط والبنود ، وجميع السياسات التي تم نشرها على مؤسسة موقع حراج للتسويق الإلكتروني وضعت لحماية وحفظ حقوق كل من ( مؤسسة موقع حراج للتسويق الإلكتروني ) و ( المستخدم الذي يصل إلى الموقع بتسجيل او من دون تسجيل )أو ( العميل المستفيد من الإعلانات بتسجيل أو من دون تسجيل).

                    تم إنشاء الاتفاقية بناء على نظام التعاملات الإلكترونية. تخضع البنود والشروط والأحكام والمنازعات القانونية للقوانين والتشريعات والأنظمة المعمول بها.

                    لكونك مستخدم فأنك توافق على الالتزام بكل ما يرد بهذه الاتفاقية في حال استخدامك للموقع او في حال الوصول اليه او في حالة التسجيل في الخدمة. يحق لموقع حراج التعديل على هذه الاتفاقية في أي وقت وتعتبر ملزمة لجميع الأطراف بعد الإعلان عن التحديث في الموقع أو في أي وسيلة آخرى.
                </div>
            </div>


        </div>
    </div>

@endsection


