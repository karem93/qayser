
<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label"> الاسم</label>
    <div class="col-lg-5{{ $errors->has('username') ? ' has-danger' : '' }}">
        {!! Form::text('username',old('username'),['class'=>'form-control m-input','autofocus','placeholder'=> 'الاسم' ]) !!}
        @if ($errors->has('username'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">الهاتف</label>
    <div class="col-lg-5{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','placeholder'=> 'الهاتف' ]) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>




</div>


<div class="form-group m-form__group row">

    <label class="col-lg-1 col-form-label">البريد الالكتروني</label>
    <div class="col-lg-5{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::text('email',old('email'),['class'=>'form-control m-input','placeholder'=> 'البريد الالكترونى' ]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">كلمة المرور </label>
    <div class="col-lg-5{{ $errors->has('password') ? ' has-danger' : '' }}">
        {!! Form::password('password',['class'=>'form-control m-input','placeholder'=> 'كلمة المرور' ]) !!}
        @if ($errors->has('password'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">الدوله</label>
    <div class="col-lg-5{{ $errors->has('country_id') ? ' has-danger' : '' }}">
        {{--{!! Form::text('phone',old('phone'),['class'=>'form-control m-input','placeholder'=> 'Phone' ]) !!}--}}
        <select name="country_id" id="country_id" class="form-control m-input" >
            <option value="">--اختر الدوله--</option>
            @foreach($countries as $country)
                <option value="{{$country->id}}" {{$user ?? '' && $user->country_id==$country->id ?'selected':''}}>{{$country->name_ar}}</option>
            @endforeach

        </select>
        @if ($errors->has('country_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('country_id') }}</strong>
            </span>
        @endif
    </div>

    <label class="col-lg-1 col-form-label">المدينه</label>
    <div class="col-lg-5{{ $errors->has('city_id') ? ' has-danger' : '' }}">
        <select name="city_id" id="city_id" class="form-control m-input" >
            <option value="">--اختر المدينه --</option>

            @if($user ?? '' && $user->country )
                @foreach($user->country->cities as $city)
                    <option value="{{$city->id}}" {{$user ?? '' && $user->city_id==$city->id ?'selected':''}}>{{$city->name_ar}}</option>

                @endforeach
            @endif


        </select>
        @if ($errors->has('city_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
        @endif
    </div>

</div>



<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">الصوره</label>
    <div class="col-lg-11{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if($user ?? '' && $user->photo)
<div class="row">




            <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                <img
                        data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                        alt="First slide [800x4a00]"
                        src="{{asset($user->photo)}}"
                        style="height: 150px; width: 150px"
                        data-holder-rendered="true">
            </div>

</div>
@endif


