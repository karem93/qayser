<div class="form-group m-form__group row">
    <label for="city_id" class="col-lg-2 col-form-label">الموقع: </label>
    <div class="col-lg-10{{ $errors->has('location') ? ' has-danger' : '' }}">
        <select name="city_id" data-url="{{url('api/get-categories/')}}" id="city_id" class="form-control city_id m-input" required>
            <option value="">قم باختيار مكان البنر </option>
            <option value="1" @if (old('location') == 1) selected="selected" @endif>الصفحة الرئيسية</option>
            <option value="2" @if (old('location') == 2) selected="selected" @endif>صفحة البحث</option>
            <option value="3" @if (old('location') == 3) selected="selected" @endif>صغحة تفاصيل الاعلان</option>
        </select>
        @if ($errors->has('location'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الصورة: </label>
    <div class="col-lg-10{{ $errors->has('photo') ? ' has-danger' : '' }}">
        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($slider) && $slider->photo)
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">
            <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{$slider->photo}}"
                    style="height: 150px; width: 150px"
                    data-holder-rendered="true">
        </div>

    </div>
@endif




