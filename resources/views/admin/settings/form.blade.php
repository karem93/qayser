<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> البريد الالكتروني </label>
    <div class="col-lg-4{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::email('email',old('email'),['class'=>'form-control m-input','autofocus','placeholder'=> 'البريد الالكترونى' ]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">الهاتف </label>
    <div class="col-lg-4{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','placeholder'=> 'الهاتف' ]) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> نبذه عننا </label>
    <div class="col-lg-4{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::text('brief',old('brief'),['class'=>'form-control m-input','autofocus','placeholder'=> 'نبذه عننا' ]) !!}
        @if ($errors->has('brief'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('brief') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-2 col-form-label">العنوان </label>
    <div class="col-lg-4{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::text('address',old('address'),['class'=>'form-control m-input','placeholder'=> 'العنوان' ]) !!}
        @if ($errors->has('address'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>

</div>



<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">انستجرام</label>
    <div class="col-lg-3{{ $errors->has('instagram') ? ' has-danger' : '' }}">
        {!! Form::text('instagram',old('instagram'),['class'=>'form-control m-input','placeholder'=> 'انستجرام' ]) !!}
        @if ($errors->has('instagram'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('instagram') }}</strong>
            </span>
        @endif
    </div>

    <label class="col-lg-1 col-form-label">فيسبوك</label>
    <div class="col-lg-3{{ $errors->has('facebook') ? ' has-danger' : '' }}">
        {!! Form::text('facebook',old('facebook'),['class'=>'form-control m-input','placeholder'=> 'فيسبوك' ]) !!}
        @if ($errors->has('facebook'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('facebook') }}</strong>
            </span>
        @endif
    </div>


    <label class="col-lg-1 col-form-label">تويتر</label>
    <div class="col-lg-3{{ $errors->has('twitter') ? ' has-danger' : '' }}">
        {!! Form::text('twitter',old('twitter'),['class'=>'form-control m-input','placeholder'=> 'تويتر' ]) !!}
        @if ($errors->has('twitter'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('twitter') }}</strong>
            </span>
        @endif
    </div>

</div>



<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">لوجو التطبيق</label>
    <div class="col-lg-11{{ $errors->has('logo') ? ' has-danger' : '' }}">

        <input type="file" name="logo" class="form-control uploadinput">
        @if ($errors->has('logo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('logo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if(isset($settings) && $settings->logo)
    <input type="hidden" value="{{ $settings->logo }}" name="logo">
    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px; text-align: center">

            <img
                data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                alt="First slide [800x4a00]"
                src="{{asset($settings->logo)}}"
                style="height: 150px; width: 150px"
                data-holder-rendered="true">
        </div>

    </div>
@endif


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> الخصوصيه باللغه العربيه: </label>
    <div class="col-lg-10{{ $errors->has('privacy_ar') ? ' has-danger' : '' }}">
        {!! Form::textarea('privacy_ar',old('privacy_ar'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'الخصوصيه باللغه العربيه' ]) !!}
        @if ($errors->has('privacy_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('privacy_ar') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> الخصوصيه باللغه الانجليزيه: </label>
    <div class="col-lg-10{{ $errors->has('privacy_en') ? ' has-danger' : '' }}">
        {!! Form::textarea('privacy_en',old('privacy_en'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'الخصوصيه باللغه الانجليزيه' ]) !!}
        @if ($errors->has('privacy_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('privacy_en') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> الشروط باللغه العربيه: </label>
    <div class="col-lg-10{{ $errors->has('terms_ar') ? ' has-danger' : '' }}">
        {!! Form::textarea('terms_ar',old('terms_ar'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'الشروط باللغه العربيه' ]) !!}
        @if ($errors->has('terms_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('terms_ar') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">

    <label class="col-lg-2 col-form-label"> الشروط باللغه الانجليزيه: </label>
    <div class="col-lg-10{{ $errors->has('terms_en') ? ' has-danger' : '' }}">
    {!! Form::textarea('terms_en',old('terms_en'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'الشروط باللغه الانجليزيه' ]) !!}
    @if ($errors->has('terms_en'))
    <span class="form-control-feedback" role="alert">
    <strong>{{ $errors->first('terms_en') }}</strong>
    </span>
    @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> من نحن باللغه العربيه: </label>
    <div class="col-lg-10{{ $errors->has('aboutus_ar') ? ' has-danger' : '' }}">
        {!! Form::textarea('aboutus_ar',old('aboutus_ar'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'من نحن باللغه العربيه' ]) !!}
        @if ($errors->has('aboutus_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('aboutus_ar') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label"> من نحن باللغه الانجليزيه: </label>
    <div class="col-lg-10{{ $errors->has('aboutus_en') ? ' has-danger' : '' }}">
        {!! Form::textarea('aboutus_en',old('aboutus_en'),['class'=>'form-control m-input summernote','autofocus','placeholder'=> 'من نحن باللغه الانجليزيه' ]) !!}
        @if ($errors->has('aboutus_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('aboutus_en') }}</strong>
            </span>
        @endif
    </div>

</div>



