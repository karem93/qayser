@extends('admin.layouts.app')

@section('title')
    تفاصيل المدير
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/admins')}}" class="m-menu__link">
            <span class="m-menu__link-text">المدراء</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">تفاصيل المدير</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل المدير
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($user,['route' => ['admins.show' , $user->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">الاسم </label>
                <div class="col-lg-5{{ $errors->has('username') ? ' has-danger' : '' }}">
                    {!! Form::text('username',old('username'),['class'=>'form-control m-input','autofocus','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">الهاتف</label>
                <div class="col-lg-5{{ $errors->has('phone') ? ' has-danger' : '' }}">
                    {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>


            </div>


            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">البريد الالكترونى</label>
                <div class="col-lg-10{{ $errors->has('email') ? ' has-danger' : '' }}">
                    {!! Form::text('email',old('email'),['class'=>'form-control m-input','disabled' ]) !!}

                </div>


            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label">الدوله</label>
                <div class="col-lg-5{{ $errors->has('address') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$user->country->name_ar}}" disabled="">

                </div>
                <label class="col-lg-1 col-form-label">المدينه</label>
                <div class="col-lg-5{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$user->city->name_ar}}" disabled="">
                </div>


            </div>


            @if(isset($user) && $user->photo)
                <div class="row">


                    <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                        <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt="First slide [800x4a00]"
                            src="{{asset($user->photo)}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">
                    </div>

                </div>
            @endif



        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

