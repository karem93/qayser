<!DOCTYPE html>
<html dir="rtl" lang="ar">

<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>  لوحة التحكم | القيصر@yield('title') </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>



    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script type="text/javascript" src="/admin/custom/js/form_select2.js"></script>
    <script type="text/javascript" src="{{url('admin/custom/js/select2.min.js')}}"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    {{--fast select--}}

    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.min.css">--}}

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/fastselect/0.7.3/fastselect.standalone.min.js"></script>--}}

    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    {{--@if(App::isLocale('en'))--}}
        {{--{!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}--}}
    {{--@else--}}
        {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.css') !!}
    {{--@endif--}}

    {{--<link href="{{ asset('admin/custom/bootstrap-fileinput-master/css/fileinput.min.css') }}" rel="stylesheet">--}}
    {{--@else--}}
    <link href="{{ asset('admin/custom/bootstrap-fileinput-master/css/fileinput-rtl.min.css') }}" rel="stylesheet">
    {{--@endif--}}

    <style>
        input{
            margin-bottom: 10px;
        }

        .group-buttons {
            vertical-align: top;
        }
    </style>

    {!! Html::style('admin/vendors/base/vendors.bundle.rtl.css') !!}
{{--    {!! Html::style('admin/demo/default/base/style.bundle.rtl.css') !!}--}}
    {!! Html::style('admin/custom/toastr/toastr-rtl.min.css') !!}
    {!! Html::style('admin/custom/css/custom-rtl.css') !!}
    {!! Html::style('admin/custom/css/jadara-custom-style.css') !!}


    <link rel="shortcut icon" href="/website/img/raptlogo.png"/>

    {{--@if(App::isLocale('en'))--}}
        {{--<link rel="stylesheet" href="{{asset('admin/demo/default/base/style.bundle.css')}}">--}}

    {{--@else--}}
        <link rel="stylesheet" href="{{asset('admin/demo/default/base/style.bundle.rtl.css')}}">
    {{--@endif--}}
    @yield('header')
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
    <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">

                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-light ">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <a href="/webadmin/dashboard" class="m-brand__logo-wrapper">
                                <img alt=""
                                     src="/website/img/raptlogo.png"/>
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">

                            <!-- BEGIN: Left Aside Minimize Toggle -->
                            <a href="javascript:;" id="m_aside_left_minimize_toggle"
                               class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:;" id="m_aside_left_offcanvas_toggle"
                               class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Header Menu Toggler -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;"
                               class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Topbar Toggler -->
                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;"
                               class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>

                            <!-- BEGIN: Topbar Toggler -->
                        </div>
                    </div>
                </div>

                <!-- END: Brand -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                    <!-- BEGIN: Horizontal Menu -->
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light "
                            id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                    <div id="m_header_menu"
                         class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
                        <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                            @yield('topBar')
                        </ul>
                    </div>

                    <!-- END: Horizontal Menu -->

                    <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                        {{--<ul class="navbar-nav ml-auto">--}}
                            {{--@if (App::isLocale('en'))--}}
                                {{--<li class="nav-item language-link">--}}
                                    {{--<a class="nav-link"--}}
                                       {{--href="/changeLang/ar"--}}
                                    {{-->AR</a>--}}
                                {{--</li>--}}
                            {{--@else--}}
                                {{--<li class="nav-item language-link">--}}
                                    {{--<a class="nav-link"--}}
                                       {{--href="/changeLang/en"--}}
                                    {{-->EN </a>--}}
                                {{--</li>--}}
                            {{--@endif--}}
                        {{--</ul>--}}
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                {{--<li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right 	m-dropdown--mobile-full-width"--}}
                                {{--m-dropdown-toggle="click"--}}
                                {{--m-dropdown-persistent="1">--}}
                                {{--<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">--}}
                                {{--<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>--}}
                                {{--<span class="m-nav__link-icon"><i class="flaticon-alarm"></i></span>--}}
                                {{--</a>--}}
                                {{--<div class="m-dropdown__wrapper">--}}
                                {{--<span class="m-dropdown__arrow m-dropdown__arrow--right"></span>--}}
                                {{--<div class="m-dropdown__inner">--}}
                                {{--<div class="m-dropdown__header m--align-center"--}}
                                {{--style="background: url({{Request::root()}}/admin/app/media/img/misc/notification_bg.jpg); background-size: cover;">--}}
                                {{--<span class="m-dropdown__header-title">9 New</span>--}}
                                {{--<span class="m-dropdown__header-subtitle">User Notifications</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-dropdown__body">--}}
                                {{--<div class="m-dropdown__content">--}}
                                {{--<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand"--}}
                                {{--role="tablist">--}}
                                {{--<li class="nav-item m-tabs__item">--}}
                                {{--<a class="nav-link m-tabs__link active" data-toggle="tab"--}}
                                {{--href="#topbar_notifications_notifications" role="tab">--}}
                                {{--Alerts--}}
                                {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="nav-item m-tabs__item">--}}
                                {{--<a class="nav-link m-tabs__link" data-toggle="tab"--}}
                                {{--href="#topbar_notifications_events" role="tab">Events</a>--}}
                                {{--</li>--}}
                                {{--<li class="nav-item m-tabs__item">--}}
                                {{--<a class="nav-link m-tabs__link" data-toggle="tab"--}}
                                {{--href="#topbar_notifications_logs" role="tab">Logs</a>--}}
                                {{--</li>--}}
                                {{--</ul>--}}
                                {{--<div class="tab-content">--}}
                                {{--<div class="tab-pane active"--}}
                                {{--id="topbar_notifications_notifications" role="tabpanel">--}}
                                {{--<div class="m-scrollable" data-scrollable="true"--}}
                                {{--data-height="250" data-mobile-height="200">--}}
                                {{--<div class="m-list-timeline m-list-timeline--skin-light">--}}
                                {{--<div class="m-list-timeline__items">--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>--}}
                                {{--<span class="m-list-timeline__text">12 new users registered</span>--}}
                                {{--<span class="m-list-timeline__time">Just now</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge"></span>--}}
                                {{--<span class="m-list-timeline__text">System shutdown <span--}}
                                {{--class="m-badge m-badge--success m-badge--wide">pending</span></span>--}}
                                {{--<span class="m-list-timeline__time">14 mins</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge"></span>--}}
                                {{--<span class="m-list-timeline__text">New invoice received</span>--}}
                                {{--<span class="m-list-timeline__time">20 mins</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge"></span>--}}
                                {{--<span class="m-list-timeline__text">DB overloaded 80% <span--}}
                                {{--class="m-badge m-badge--info m-badge--wide">settled</span></span>--}}
                                {{--<span class="m-list-timeline__time">1 hr</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge"></span>--}}
                                {{--<span class="m-list-timeline__text">System error - <a--}}
                                {{--href="#"--}}
                                {{--class="m-link">Check</a></span>--}}
                                {{--<span class="m-list-timeline__time">2 hrs</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item m-list-timeline__item--read">--}}
                                {{--<span class="m-list-timeline__badge"></span>--}}
                                {{--<span href="" class="m-list-timeline__text">New order received <span--}}
                                {{--class="m-badge m-badge--danger m-badge--wide">urgent</span></span>--}}
                                {{--<span class="m-list-timeline__time">7 hrs</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item m-list-timeline__item--read">--}}
                                {{--<span class="m-list-timeline__badge"></span>--}}
                                {{--<span class="m-list-timeline__text">Production server down</span>--}}
                                {{--<span class="m-list-timeline__time">3 hrs</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge"></span>--}}
                                {{--<span class="m-list-timeline__text">Production server up</span>--}}
                                {{--<span class="m-list-timeline__time">5 hrs</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tab-pane" id="topbar_notifications_events"--}}
                                {{--role="tabpanel">--}}
                                {{--<div class="m-scrollable" data-scrollable="true"--}}
                                {{--data-height="250" data-mobile-height="200">--}}
                                {{--<div class="m-list-timeline m-list-timeline--skin-light">--}}
                                {{--<div class="m-list-timeline__items">--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>--}}
                                {{--<a href="" class="m-list-timeline__text">New--}}
                                {{--order received</a>--}}
                                {{--<span class="m-list-timeline__time">Just now</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>--}}
                                {{--<a href="" class="m-list-timeline__text">New--}}
                                {{--invoice received</a>--}}
                                {{--<span class="m-list-timeline__time">20 mins</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>--}}
                                {{--<a href="" class="m-list-timeline__text">Production--}}
                                {{--server up</a>--}}
                                {{--<span class="m-list-timeline__time">5 hrs</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>--}}
                                {{--<a href="" class="m-list-timeline__text">New--}}
                                {{--order received</a>--}}
                                {{--<span class="m-list-timeline__time">7 hrs</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>--}}
                                {{--<a href="" class="m-list-timeline__text">System--}}
                                {{--shutdown</a>--}}
                                {{--<span class="m-list-timeline__time">11 mins</span>--}}
                                {{--</div>--}}
                                {{--<div class="m-list-timeline__item">--}}
                                {{--<span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>--}}
                                {{--<a href="" class="m-list-timeline__text">Production--}}
                                {{--server down</a>--}}
                                {{--<span class="m-list-timeline__time">3 hrs</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="tab-pane" id="topbar_notifications_logs"--}}
                                {{--role="tabpanel">--}}
                                {{--<div class="m-stack m-stack--ver m-stack--general"--}}
                                {{--style="min-height: 180px;">--}}
                                {{--<div class="m-stack__item m-stack__item--center m-stack__item--middle">--}}
                                {{--<span class="">All caught up!<br>No new logs.</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</li>--}}
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                    m-dropdown-toggle="click">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="/website/img/raptlogo.png"
                                                         class="m--img-rounded m--marginless" alt=""/>
												</span>
                                        {{--<span class="m-topbar__username m--hide">Nick</span>--}}
                                    </a>
                                    <div class="m-dropdown__wrapper">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center"
                                                 style="background: url({{Request::root()}}/admin/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                                                <div class="m-card-user m-card-user--skin-light">
                                                    <div class="m-card-user__pic">
                                                        {{--<img src="{{Request::root()}}/admin/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt=""/>--}}
                                                        <span class="m-type m-type--lg m--bg-danger"><span class="m--font-light admin-text-size">
{{--                                         {{auth()->user()->name}}--}}
                                                        </span>

                                                    </div>
                                                    <div class="m-card-user__details">
                                                        <span class="m-card-user__name m--font-weight-500">
                                                            {{auth()->user()->username}}
                                                        </span>
                                                        <a href="" class="m-card-user__email m--font-weight-300 m-link">
                                                            {{auth()->user()->email}}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">

                                                        <li class="m-nav__item">
                                                            <a href="/webadmin/logout"
                                                               class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">تسجيل الخروج</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>

                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </header>

    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">

            <!-- BEGIN: Aside Menu -->
            <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light "
                 m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                    @include('admin.layouts.side')
                </ul>
            </div>

            <!-- END: Aside Menu -->
        </div>

        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-content">
                {{--                @include('admin.alert')--}}
                @yield('content')
            </div>
        </div>
    </div>

    <!-- end:: Body -->

    <!-- begin::Footer -->
    <footer class="m-grid__item		m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">

            </div>
        </div>
    </footer>
    <!-- end::Footer -->
</div>

<!-- end:: Page -->

<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
                </div>

                <!-- end::Scroll Top -->


                <!--begin::Global Theme Bundle -->
            {!! Html::script('admin/vendors/base/vendors.bundle.js') !!}
            {!! Html::script('admin/demo/default/base/scripts.bundle.js') !!}
            <!--end::Global Theme Bundle -->

                <!--begin::Page Scripts -->
            {!! Html::script('admin/app/js/dashboard.js') !!}
            {!! Html::script('admin/custom/js/custom.js') !!}
            {!! Html::script('admin/custom/toastr/toastr.min.js') !!}
                <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
            <!--end::Page Scripts -->
                <!--begin::Page Vendors -->
                @yield('footer')
                <script src="{{ asset('admin/custom/bootstrap-fileinput-master/js/fileinput.min.js') }}"></script>
                <script src="{{ asset("admin/custom/js/pages/form_multiselect.js") }}"></script>

                {!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}
{{--                @if(App::isLocale('ar'))--}}

                    {!! Html::script('admin/custom/js/testArea/script.js') !!}
                {{--@else--}}
                    {{--{!! Html::script('admin/custom/js/testArea/script.en.js') !!}--}}

                {{--@endif--}}
                <script src="/admin/app/js/countries.js"></script>
                <script>
                    $('.summernote').summernote({});
                </script>


                <script>
                    var main_image = $("#main_image").attr("data-url");
                    //fast select
                    //    $('.multipleSelect').fastselect();

                    // upload product image_type_to_extension
                    $(".uploadinput").fileinput({
                        rtl: true,
                        dropZoneEnabled: false,
                        maxFileSize: 5120,
                        allowedFileExtensions: ["jpg", "png", "gif", "JPG", "JPEG", "jpeg","pdf","svg"],
                        showUpload: false
                    });

                    // clone input
                    $(".plusebtn").on('click', function(){
                        var ele = $('.featureinputminus').clone().attr("class", "newfeatureinput row");
                        ele.children().find('select').removeAttr("disabled");
                        ele.children().find('input').removeAttr("disabled");
                        ele.appendTo('.allfeatures');
                    });
                    $(document).on('click',".minusbtn", function(){
                        $(this).parents('.newfeatureinput').remove();
                    });
                    $(document).on('click',".removetabletr", function(){
                        $(this).parents('tr').remove();
                    });
                    $("#main_image").fileinput({
                        initialPreview: [main_image],
                        initialPreviewAsData: true,
                        initialPreviewConfig: [
                            {downloadUrl: main_image, size: 930321, width: "120px", key: 1},
                        ],
                        overwriteInitial: true,
                        maxFileSize: 1024,
                        allowedFileExtensions: ["jpg", "jpeg", "JPG", 'JPEG','pdf'],
                        rtl: true,
                    });
                    //    });
                </script>

                <div class="d-none">
                    <span id="d-root" data-root="{{Request::url()}}"></span>
                    <span id="d-fmsg">{!! Session::get("flash_message") !!}</span>
                    <span id="d-csrf">{{csrf_token()}}</span>
                </div>

                @stack('js')

</body>

<!-- end::Body -->
</html>
