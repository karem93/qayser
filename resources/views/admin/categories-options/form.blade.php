<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم خاصيه القسم بالعربية: </label>
    <div class="col-lg-10{{ $errors->has('name_ar') ? ' has-danger' : '' }}">
        {!! Form::text('name_ar',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم القسم بالعربية"]) !!}
        @if ($errors->has('name_ar'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_ar') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">اسم خاصيه القسم بالانجليزية: </label>
    <div class="col-lg-10{{ $errors->has('name_en') ? ' has-danger' : '' }}">
        {!! Form::text('name_en',null,['class'=>'form-control m-input','autofocus','placeholder'=>"اسم القسم بالانجليزية"]) !!}
        @if ($errors->has('name_en'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">الايقونة: </label>
    <div class="col-lg-10{{ $errors->has('icon') ? ' has-danger' : '' }}">

        <input type="file" name="icon" class="form-control uploadinput">
        @if ($errors->has('icon'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('icon') }}</strong>
            </span>
        @endif
    </div>

</div>



@if(isset($option) && $option->icon)
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">
            <img
                    data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                    alt="First slide [800x4a00]"
                    src="{{$option->icon}}"
                    style="height: 150px; width: 150px"
                    data-holder-rendered="true">
        </div>
    </div>
@endif






<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">قيم الخاصيه </label>
    <div class="col-lg-10{{ $errors->has('parent_id') ? ' has-danger' : '' }}">
        <div class="table-responsive">
            <table class="table m-0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>القيمه</th>
                    <th> <a href="javascript:void(0)" onClick="addInput('table_dynamic');" class="btn btn-sm  btn-dark float-left"><span class="fa fa-plus fa-2x"></span></a></th>
                </tr>
                </thead>
                <tbody id="table_dynamic">
                @foreach($values ??[] as $value)
                <tr>
                    <td># <input type="hidden" name="value_id[]" value="{{$value->id}}"></td>
                    <td><input name="value[]"  class="form-control" value="{{$value->value}}" type="text" ></td>
                    <td></td>
                </tr>
                @endforeach

                <tr>
                    <td>#</td>
                    <td><input name="value[]"  class="form-control" type="text"></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>


    </div>
</div>






@section('footer')
    <script type="text/javascript">
        let counter = 1;
        let limit = 99;
        function addInput(divName){
            if (counter == limit)  {
                alert("You have reached the limit of adding " + counter + " inputs");
            }
            else {
                let newdiv = document.createElement('tr');
                newdiv.innerHTML = "<td>#</td><td><input name='value[]'  class='form-control' type='text' ></td>" +
                "<td></td>";
                document.getElementById(divName).appendChild(newdiv);
                counter++;
            }
        }

    </script>
@endsection


