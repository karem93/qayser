@extends('admin.layouts.app')
@section('title')
    الاقسام الفرعيه
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item active-top-bar">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">الاقسام الفرعيه</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>

@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        الاقسام الفرعيه
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div><a href="{{route('subcategories.create')}}" style="margin-bottom:20px"
                    class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i> اضف قسم فرعي</a></div>


            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_country">
                <thead>
                <tr>
                    <th>#</th>
                    <th>اسم القسم بالعربية</th>
                    <th>اسم القسم بالانجليزية</th>
                    <th>القسم التابع له</th>
                    <th>الاجراءات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $index=>$category)
                    <tr>
                        <td>{{$index + $categories->firstItem()}}</td>
                        <td>{{$category->name_ar}}</td>
                        <td>{{$category->name_en}}</td>
                        <td>{{$category->parent ? $category->parent->name_ar : '-----------'}}</td>
                        <td>
                            <a title="تعديل" href="/webadmin/subcategories/{{$category->id}}/edit"><i
                                    class="fa fa-edit"></i></a>
                            <form class="inline-form-style"
                                  action="/webadmin/subcategories/{{ $category->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$categories->links()}}
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/countrcountriess!!}--}}
@endsection
