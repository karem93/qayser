<section id="video-container" class="hide-in-sm">
    <div class="video-overlay"></div>
    <div class="video-content">
        <div class="inner">
            <span></span>
            <h2>اعثر على أفضل الصفقات في أي وقت ومن أي مكان</h2><br/>
            <div class="download">
                <a href="#"><img src="/website/img/android.png"></a>
                <a href="#"><img src="/website/img/apple.png"></a>
            </div>
        </div>
    </div>
    <video autoplay="" loop="" muted>
        <source src="highway-loop.mp4" type="/website/video/mp4"/>
    </video>
</section>
<footer class="hide-in-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="about-veno">
                    <div class="logo">
                        <img src="/website/img/kayser_logo.png" alt="Venue Logo" width="140px">
                    </div>
                    <ul class="social-icons">
                        <li>
                            <a href="{{$setting->facebook}}"><i class="fa fa-facebook"></i></a>
                            <a href="{{$setting->twitter}}"><i class="fa fa-twitter"></i></a>
                            <a href="{{$setting->instagram}}"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="useful-links">
                    <div class="footer-heading">
                        <h4>معلومات الشركه</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                <li><a href="#"><i class="fa fa-stop"></i>من نحن</a></li>
                                <li><a href="#"><i class="fa fa-stop"></i>للاعلان على فرصة</a></li>
                                <li><a href="#"><i class="fa fa-stop"></i>شروط الاستخدام</a></li>
                                <li><a href="#"><i class="fa fa-stop"></i>سياسة الخصوصية?</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="contact-info">
                    <div class="footer-heading">
                        <h4>بيانات الاتصال</h4>
                    </div>
                    <ul>
                        <li><span>رقم الجوال :</span><a href="{{$setting->phone}}">{{$setting->phone}}</a></li>
                        <li><span>الايميل :</span><a href="{{$setting->email}}">{{$setting->email}}</a></li>
                        <li><span>العنوان :</span><a href="#">{{$setting->address}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>


<br>
<br>


<div class="fixed-footer">
    <ul>
        <li><a href="/"><i class="fa fa-home"></i> الرئيسية</a></li>
        <li><a href=""><i class="fa fa-search"></i>ابحث الان</a></li>
        <li><a href="add-ad"><i class="fa fa-home"></i>اضف اعلان</a></li>
        <li><a href="profile"><i class="fa fa-home"></i>حسابي</a></li>
    </ul>
</div>
