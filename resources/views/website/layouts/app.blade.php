<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>القيصر - Kayser</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('/website/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/fontAwesome.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/hero-slider.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/owl-carousel.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/templatemo-style.css')}}">
    <link rel="stylesheet" href="{{asset('/website/css/custom.css')}}">
    <!-- link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet"> -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Almarai&display=swap" rel="stylesheet">
    <script src="{{asset('/website/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>

    @stack('css')
</head>

<body>


@include('website.layouts.header')
@include('website.layouts.messages')


@yield('content')

@include('website.layouts.footer')


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
<script>window.jQuery || document.write('<script src="{{asset('/website/js/vendor/jquery-1.11.2.min.js')}}"><\/script>')</script>

<script src="{{asset('/website/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('/website/js/datepicker.js')}}"></script>
<script src="{{asset('/website/js/plugins.js')}}"></script>
<script src="{{asset('/website/js/main.js')}}"></script>
<script src="{{asset('/admin/app/js/countries.js')}}"></script>

@stack('js')

</body>
</html>
