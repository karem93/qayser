@extends("website.layouts.app")
@section('content')

    <div class="container">
        <h3 class=" text-center">المحادثات</h3>
        <div class="messaging">
            <div class="inbox_msg">
                <div class="inbox_people">
                    <div class="headind_srch">
                        <div class="recent_heading">
                            <h4>محادثاتي</h4>
                        </div>
                    </div>
                    <div class="inbox_chat">
                        @foreach($received_messages as $index=> $message)
                        <div class="chat_list {{$index==0?'active_chat':''}}" sender-id="{{$message->sender_id}}" receiver-id="{{$message->receiver_id}}" user-id="{{$user->id}}">
                            <div class="chat_people">
                                <div class="chat_img"> <img src="{{$message->sender->photo?:'https://ptetutorials.com/images/user-profile.png'}}" alt="sunil"> </div>
                                <div class="chat_ib">
                                    <h5>{{$message->sender->id==auth()->id()?$message->receiver->username:$message->sender->username}} <span class="chat_date">{{$message->created_at->diffForHumans()}}</span></h5>
                                    <p>{{$message->message}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
                <div class="mesgs" id="mesgs">
                    <div class="msg_history" id="msg_history">


                        @foreach($recent_chat as $chat)
                        <div class="{{$chat->sender->id==auth()->id()?'outgoing_msg':'incoming_msg'}}">
                            @if($chat->sender->id!=auth()->id())
                            <div class="incoming_msg_img"> <img src="{{$chat->sender->id==auth()->id()?($chat->sender->photo?:'https://ptetutorials.com/images/user-profile.png'):($chat->receiver->photo?:'https://ptetutorials.com/images/user-profile.png')}}" alt="sunil"> </div>
                            @endif
                            <div class="{{$chat->sender->id==auth()->id()?'sent_msg':'received_msg'}}">
                                <div class="{{$chat->sender->id!=auth()->id()?'received_withd_msg':''}}">
                                    <p>{{$chat->message}}</p>
                                    <span class="time_date">{{$chat->created_at->format('H:i:s a')}}    |    {{$chat->created_at->format('M d')}}</span></div>
                            </div>
                        </div>
                       @endforeach
                    </div>
                    @if($recent_chat->isNotEmpty())
                    <div class="type_msg">
                        <div class="input_msg_write">
                            <input type="text" class="write_msg" value="" placeholder="Type a message" />
                            <button class="msg_send_btn" sender-id="{{$user->id}}" receiver-id="{{$recent_chat[0]->receiver_id==$user->id?$recent_chat[0]->sender_id:$recent_chat[0]->receiver_id}}" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                        </div>
                    </div>
                    @endif
                </div>
            </div>



        </div></div>

@endsection