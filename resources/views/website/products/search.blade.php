@extends("website.layouts.app")
@section('content')
    @if(isset($sliders) && count($sliders) > 0)
        <section>
            <div class="container">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        @foreach($sliders as $slider)
                            <div class="item active">
                                <img src="{{$slider->photo}}" alt="Los Angeles" style="width:100%; height: 250px">
                            </div>
                        @endforeach
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section>
    @endif
    <div class="container hide-in-sm">
        <section class="banner" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 pull-left">
                        <div class="banner-caption">
                            <div class="line-dec"></div>
                            <span>من خلال فرصة بإمكانك البحث عن وظيفة وبيع وشراء أي شيء</span>
                            <!-- <div class="add-adv">
                                <a href="#">اضف اعلانك الان</a>
                            </div> -->
                        </div>
                        <div class="submit-form">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="margin-bottom: 13px;">

                                <li class="nav-item active">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills1"
                                       role="tab" aria-controls="pills-profile" aria-selected="false">كل الاعلانات</a>
                                </li>


                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                                       role="tab" aria-controls="pills-profile" aria-selected="false">سيارات</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">عقارات</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills2"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">اجهزة</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills3"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">مواشي وطيور</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills4"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">اثاث</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills5"
                                       role="tab" aria-controls="pills-contact" aria-selected="false">كل اكسبرس</a>
                                </li>

                            </ul>
                            <div class="tab-content" id="pills-tabContent">

                                <div class="tab-pane fade in active" id="pills1" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
                                    <form action="/search" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-9 first-item">
                                                <fieldset>
                                                    <input name="search" type="text" class="form-control" id="name"
                                                           placeholder="ابحث عن" >
                                                </fieldset>
                                            </div>

                                            <div class="col-md-3">
                                                <fieldset>
                                                    <button type="submit" id="form-submit" class="btn">ابحث</button>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                                {{--<div class="tab-pane fade in" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">--}}

                                {{--<form>--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-md-4 first-item">--}}
                                {{--<fieldset>--}}
                                {{--<select>--}}
                                {{--<option selected="selected">كل المدن</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--</select>--}}
                                {{--</fieldset>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4 first-item">--}}
                                {{--<fieldset>--}}
                                {{--<select>--}}
                                {{--<option selected="selected">الموقع بالكامل</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--</select>--}}
                                {{--</fieldset>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-4 first-item">--}}
                                {{--<fieldset>--}}
                                {{--<select>--}}
                                {{--<option selected="selected">جميع الفئات</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--<option>ألرياض</option>--}}
                                {{--</select>--}}
                                {{--</fieldset>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<br>--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-md-9 first-item">--}}
                                {{--<fieldset>--}}
                                {{--<input name="name" type="text" class="form-control" id="name" placeholder="ابحث عن" required="">--}}
                                {{--</fieldset>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-3">--}}
                                {{--<fieldset>--}}
                                {{--<button type="submit" id="form-submit" class="btn">ابحث</button>--}}
                                {{--</fieldset>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">--}}
                                {{--<form action="/search" method="post">--}}
                                {{--@csrf--}}

                                {{--<div class="row">--}}
                                {{--<div class="col-md-9 first-item">--}}
                                {{--<fieldset>--}}
                                {{--<input name="title" type="text" class="form-control" id="name" placeholder="ابحث عن" required="">--}}
                                {{--</fieldset>--}}
                                {{--</div>--}}

                                {{--<div class="col-md-3">--}}
                                {{--<fieldset>--}}
                                {{--<button type="submit" id="form-submit" class="btn">ابحث</button>--}}
                                {{--</fieldset>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</form>--}}
                                {{--</div>--}}

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <br>
    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-3 filter">

                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                    <div class="form-group row ">
                        <div class="col-sm-12">
                            <select class="form-control" id="filter_city_id" name="city_id">
                                <option value="" selected="selected">كل المدن</option>


                                @foreach($cities as $city)
                                    <option value="{{$city->id}}" {{session('city_id')==$city->id?'selected':''}}>{{$city->name_ar}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="col-sm-12">
                            <select class="form-control" id="category_id" name="category_id">
                                <option value="" selected="selected">اختر القسم</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name_ar}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="col-sm-12">
                            <select class="form-control" id="subcategory_id" name="subcategory_id">
                                <option selected="selected" value="">نوع الاعلان</option>

                            </select>
                        </div>


                        <div class="col-sm-12">
                            <select class="form-control" id="type" name="type">
                                <option selected="selected" value="">مواصفات الاعلان</option>

                            </select>
                        </div>
                        <div class="form-group  row" id="options_div" style="display: none">
                            <label class="col-sm-2 col-form-label">خصائص القسم </label>
                            <div class="col-sm-10">
                                <div class="table-responsive">
                                    <table class="table m-0">
                                        <thead>
                                        <tr>
                                            <th width="10%">#</th>
                                            <th width="10%">الخاصيه</th>
                                            <th>القيمه</th>
                                        </tr>
                                        </thead>
                                        <tbody id="table_dynamic">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-12 col-form-label">السعر</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="price_start" name="price_start"
                                   placeholder="من">
                        </div>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="price_end" name="price_end" placeholder="الي">
                        </div>
                    </div>


                    <br>

                    {{--<div class="form-group row">--}}
                    {{--<div class="col-sm-12">شارات</div>--}}
                    {{--<div class="col-sm-12">--}}
                    {{--<div class="form-check">--}}
                    {{--<input class="form-check-input" type="checkbox" id="gridCheck1">--}}
                    {{--<label class="form-check-label" for="gridCheck1">--}}
                    {{--بيانات الصيانة الكاملة--}}
                    {{--</label>--}}
                    {{--</div>--}}

                    {{--<div class="form-check">--}}
                    {{--<input class="form-check-input" type="checkbox" id="gridCheck1">--}}
                    {{--<label class="form-check-label" for="gridCheck1">--}}
                    {{--تحت الضمان--}}
                    {{--</label>--}}
                    {{--</div>--}}

                    {{--<div class="form-check">--}}
                    {{--<input class="form-check-input" type="checkbox" id="gridCheck1">--}}
                    {{--<label class="form-check-label" for="gridCheck1">--}}
                    {{--طارئ--}}
                    {{--</label>--}}
                    {{--</div>--}}

                    {{--<div class="form-check">--}}
                    {{--<input class="form-check-input" type="checkbox" id="gridCheck1">--}}
                    {{--<label class="form-check-label" for="gridCheck1">--}}
                    {{--قابل للتفاوض--}}
                    {{--</label>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}



                    {{--<div class="form-group row">--}}
                    {{--<div class="col-sm-12">تقرير تاريخ وحالة السيارة</div>--}}
                    {{--<div class="col-sm-12">--}}
                    {{--<div class="form-check">--}}
                    {{--<input class="form-check-input" type="checkbox" id="gridCheck1">--}}
                    {{--<label class="form-check-label" for="gridCheck1" style="display: inline;">--}}
                    {{--اظهر إعلانات السيارات التي تحتوي سجل تاريخ السيارة--}}
                    {{--</label>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}


                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button class="btn btn-primary" id="searchFilter">ابحث الان</button>
                        </div>
                    </div>


                </div>
                <div class="col-md-9">
                    <div class="section-heading">
                        {{--<span>اكسبريس ديليفري سيرفس</span>--}}
                        <h2>نتائج البحث</h2>
                    </div>
                </div>
            </div>
            @if($products->isNotEmpty())
                <div class="" dir="ltr" id="search_results">
                    @foreach($products as $product)
                        <div class="col-md-3 col-xs-6 popular-item">
                            <div class="thumb">
                                <a href="/product/{{$product->id}}"><img
                                        src="{{count($product->ProductImage) > 0 ? $product->ProductImage[0]->image : ''}}"
                                        alt=""></a>
                                <div class="text-content">
                                    <h4>{{$product->title}}</h4>
                                    <span>{{$product->price}} ريال</span>
                                </div>

                            </div>
                        </div>
                    @endforeach


                </div>
            @else
                <span class="text-danger text-bold">لا توجد نتائج</span>
            @endif
        </div>
    </section>
@endsection
@push('js')
<script>



    $('#subcategory_id').on('change', function () {
        $('#options_div').hide(200);
        $('#table_dynamic').html('');
        if ($(this).val()) {
            LoadOptions.call(this);
        }
    });

    function LoadOptions(category) {
        let url = '{{url('api/get-category-options/')}}/' + $(this).val();
        $.ajax({
            url: url,
            beforeSend: function (request) {
                request.setRequestHeader("Accept-Language", 'ar');
            },
        }).done(function (data) {
            if (data.length > 0) {
                $('#options_div').show(200);
            }
            $.each(data, function (index, val) {
                let newdiv = document.createElement('tr');

                let values = [];
                $.each(val.option, function (index, value) {
                    values.push('<option value="' + value.id + '">' + value.name + '</option>')

                });
                newdiv.innerHTML = "<td>#<input type='hidden' readonly name='option_id[]' class='option_ids' value=" + val.id + "></td>" +
                    "<td>" + val.name + "</td>" +
                    "<td>" +
                    "<select class='form-control option_values' name='value[]'><option value='' >قم بالاختيار </option>" +
                    values +
                    "</select></td>";
//
//
//                    newdiv.innerHTML = "<td>#<input type='hidden' readonly name='option_id[]' value=" + val.id + "></td>" +
//                        "<td>" + val.name + "</td>" +
//                        "<td><input name='value[]' class='form-control' type='text' ></td>";
                document.getElementById('table_dynamic').appendChild(newdiv);
            });
        });
    }
</script>


@endpush
