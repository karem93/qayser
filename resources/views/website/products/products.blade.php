@extends("website.layouts.app")
@section('content')
    @if(isset($sliders) && count($sliders) > 0)
        <section>
            <div class="container">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        @foreach($sliders as $slider)
                            <div class="item active">
                                <img src="{{$slider->photo}}" alt="Los Angeles" style="width:100%; height: 250px">
                            </div>
                        @endforeach
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section>
    @endif
    <div class="container hide-in-sm">
        <section class="banner" id="top">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 pull-left">
                        <div class="banner-caption">
                            <div class="line-dec"></div>
                            <span>من خلال  فرصة بإمكانك البحث عن وظيفة وبيع وشراء أي شيء</span>
                            <!-- <div class="add-adv">
                                <a href="#">اضف اعلانك الان</a>
                            </div> -->
                        </div>
                        <div class="submit-form">
                            <div class="tab-content" id="pills-tabContent">

                                <div class="tab-pane fade in active" id="pills1" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
                                    <form action="/search" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="col-md-9 first-item">
                                                <fieldset>
                                                    <input name="search" type="text" class="form-control" id="name"
                                                           placeholder="ابحث عن" >
                                                </fieldset>
                                            </div>

                                            <div class="col-md-3">
                                                <fieldset>
                                                    <button type="submit" id="form-submit" class="btn">ابحث</button>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="popular-places" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>الاعلانات</h2>
                    </div>
                </div>
            </div>
            @if($products->isNotEmpty())
                <div class="" dir="ltr">
                    @foreach($products as $product)
                        <div class="col-md-3 col-xs-6 popular-item">
                            <div class="thumb">
                                <a href="/product/{{$product->id}}"><img
                                        src="{{count($product->ProductImage) > 0 ? $product->ProductImage[0]->image : ''}}"
                                        alt=""></a>
                                <div class="text-content">
                                    <h4>{{$product->title}}</h4>
                                    @if($product->price)
                                        <span>{{$product->price}} ريال</span>
                                    @endif

                                </div>

                            </div>
                        </div>
                    @endforeach


                </div>
            @else
                <span class="text-danger text-bold">لا توجد نتائج</span>
            @endif
        </div>
    </section>
@endsection
