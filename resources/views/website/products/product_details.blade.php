@extends("website.layouts.app")
@section('content')

    <div class="single-page">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="title">{{$product->title}}</h3>

                    <div class="imgs-single">
                        @if(count($product->ProductImage) > 0)
                            <img src="{{asset($product->ProductImage[0]->image)}}" alt="">
                        @else
                            <img src="{{asset('/website/img/kayser_logo.png')}}" alt="">
                        @endif
                    </div>

                    <div class="info-short">
                        <ul>
                            <li><i class="fa fa-building"></i>{{$product->user->username}}</li>
                            <li><i class="fa fa-map-marker"></i>{{$product->category->name_ar}}</li>
                            <li><i class="fa fa-clock-o"></i> {{$product->created_at->diffForHumans()}}</li>
                            <li><i class="fa fa-eye"></i>{{ $product->views }} مشاهدة</li>
                        </ul>
                    </div>

                <!--    <div class="full-info">-->
                <!--        <h4>خصائص الاعلان</h4>-->
                <!--        @foreach($product->ProductOption as $option)-->
                <!--            <p>- {{$option->option->name_ar}}: {{$option->optionValue->value}}</p>-->
                <!--        @endforeach-->

                <!--        <br>-->

                <!--       </div>-->
                       
                       
                <!--     <div class="full-info">-->
                <!--            <h4>وصف الاعلان</h4>-->
                <!--        <p>{{$product->description}}</p>-->

                <!--        <br>-->
                <!--       </div>-->

                <!--        @if($product->show_phone == 1)-->
                <!--            <div class="full-info">-->
                <!--                <h4>للتواصل مع المعلن</h4>-->
                <!--                <h5><i class="fa fa-mobile"></i> <a href="tel:{{$product->phone}}"> {{$product->phone}}</a>-->
                <!--                </h5>-->
                <!--            </div>-->
                <!--        @endif-->

                <!--    </div>-->


                <!--    @if(count($product->ProductImage) > 0)-->
                <!--        <div class="imgs-single">-->
                <!--            @foreach($product->ProductImage as $image)-->
                <!--                <img src="{{$image->image}}">-->
                <!--            @endforeach-->
                <!--        </div>-->
                <!--    @endif-->
                <!--</div>-->
                
                <div class="full-info">
                        @foreach($product->ProductOption as $option)

                            <p><img src="{{$option->option->icon}}" width="40px" height="40px"> {{$option->option->name_ar}}: {{$option->optionValue->value}}</p>
                        @endforeach

                    </div>

                    <div class="clearfix"></div>

                    <div class="full-text">
                        <h4>الوصف</h4>
                        <h5>{{ 	$product->description }}</h5>
                    </div>


                    @if(count($product->ProductImage) > 0)
                        <div class="imgs-single">
                            @foreach($product->ProductImage as $image)
                                <img src="{{$image->image}}">
                            @endforeach
                        </div>
                    @endif
                </div>

                <div class="col-md-4">
                    <div class="block">
                        <h4>إعلانات مشابهة</h4>
                        <ul>
                            @if(count($similar_products) > 0)
                                @foreach($similar_products as $product)
                                    <li><a href="/product/{{$product->id}}">
                                            @if(count($product->ProductImage) > 0)
                                                <img src="{{asset($product->ProductImage[0]->image)}}" alt="">
                                            @else
                                                <img src="{{asset('/website/img/kayser_logo.png')}}" alt="">
                                            @endif
                                        </a>
                                        <a href="/product/{{$product->id}}">{{$product->title}}</a>
                                    </li>
                                @endforeach
                            @else
                                <li>
                                    لا توجد اعلانات
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
