<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//authed routes
Route::group(['namespace' => 'Api', 'middleware' => [/*'auth:api',*/ 'language']], function () {
    Route::post('add-product', 'ProductsController@AddProduct');
    Route::post('edit-product', 'ProductsController@EditProduct');
    Route::delete('delete-product/{id}', 'ProductsController@DeleteProduct');
    Route::get('get-product/{id}', 'ProductsController@getProductDetails');

});


Route::group(['namespace' => 'Api', 'middleware' => 'language'], function () {

    // lists
    Route::get('get_countries', 'HomeController@getCountries');
    Route::get('get_cities/{id}', 'HomeController@getCities');
    // user
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
    Route::post('active_code', 'UserController@activeUser');
    Route::post('user_resend_code', 'UserController@resendUserActivationCode');
    Route::get('logout', 'UserController@logout');
    Route::get('get_settings', 'HomeController@getSettings');

    Route::get('get_profile', 'UserController@getProfile');
    Route::post('update_profile', 'UserController@updateProfile');
//    Route::get('notifiable', 'UserController@notifiable');
    Route::post('update_user_token', 'UserController@updateUserFCMToken');

    Route::post('/get_reset_token', 'UserController@getResetToken');
    Route::post('/check_reset_token', 'UserController@checkResetToken');
    Route::post('/reset', 'UserController@reset');
    Route::post('/update_password', 'UserController@updatePassword');
    //fav
    Route::post('add_to_fav', 'FavsController@addToFav');
    Route::get('get_favs', 'FavsController@getFavs');
    Route::get('get_my_products', 'ProductsController@getMyProducts');


    // home page
    Route::get('get-home', 'HomeController@getHome');

    // notifications
    Route::get('get_notifications', 'UserController@getNotifications');
    Route::get('/get_unread_notifications', 'UserController@unreadNotifications');
    Route::get('/delete_notifications', 'UserController@deleteNotification');
    Route::get('/delete_one_notification/{id}', 'UserController@deleteOneNotification');


    //    get category and sub category
    Route::get('get-category-options/{id}', 'ProductsController@getCategoryOptions');
    Route::post('get-category-options', 'ProductsController@getCategoryOptionsSabryUpdate');
    //used in website add product to get options of category
    Route::get('get-category-options', 'ProductsController@getCategoryOptionsSabryUpdate');


    Route::get('get-categories/{id}', 'ProductsController@getCategories');
    Route::post('search-products', 'ProductsController@SearchProduct');
    Route::get('get_cat_products/{cat_id}', 'ProductsController@getCatProduct');

    Route::post('/send_message', 'MessagesController@sendingMessage');
    Route::post('/chat_details', 'MessagesController@chatDetails');
    Route::get('/get_received_messages', 'MessagesController@getReceivedMessages');

    Route::post('search_products_by_name', 'ProductsController@SearchProductByName');
    Route::get('get_product_owner/{product_id}', 'ProductsController@getProductOwner');

});




