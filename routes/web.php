<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;


Auth::routes();
Route::get('/redirect/{provider}', 'SocialAuthController@redirect');
Route::get('/callback/{provider}', 'SocialAuthController@callback');
Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'website\HomeController@home');
});


Route::group(['prefix' => 'webadmin', 'middleware' => ['webadmin']], function () {
    Route::get('/dashboard', 'admin\DashboardController@index');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::resource('/users', 'admin\UsersController');
    Route::resource('/admins', 'admin\AdminsController');
    Route::resource('/settings', 'admin\SettingController');
    Route::resource('/categories', 'admin\CategoriesController');
    Route::resource('/subcategories', 'admin\SubCategoriesController');
    Route::resource('/categories-options', 'admin\CategoriesOptionsController');
    Route::resource('/countries', 'admin\CountryController');
    Route::resource('/cities', 'admin\CitiesController');
    Route::resource('/products', 'admin\ProductsController');
    Route::get('/get-product-category-data/{product_id}/{category_id}', 'admin\ProductsController@ProductData');
    Route::resource('/sliders', 'admin\SlidersController');


});
Route::post('/search', 'website\ProductsController@search');
Route::get('/categories/{id}', 'website\ProductsController@getSubCategories');
Route::get('/product/{id}', 'website\ProductsController@getProduct');
Route::get('/products/{cat_id}', 'website\ProductsController@getProducts');
Route::post('/search-filter', 'website\ProductsController@searchFilter');


Route::group(['middleware' => ['auth']], function () {
    Route::get('add-ad', 'website\ProductsController@AddProduct');
    Route::post('/add-ad', 'website\ProductsController@SaveProduct');
    Route::post('/add-ad/image-ajax-upload', 'website\ProductsController@uploadAdImage');
    Route::get('/profile', 'website\UsersController@profile');
    Route::get('/favourites', 'website\UsersController@favourites');
    Route::get('/chats', 'website\UsersController@chats');
});
Route::get('/information/create/ajax-countries', function () {
    $country_id = $_GET['country_id'];
    $cities = \App\Models\City::where('country_id', '=', $country_id)->get();
    return json_encode($cities);

});
Route::get('/information/create/ajax-categories', function () {
    $category_id = $_GET['category_id'];
    $subcategories = \App\Models\Category::where('parent_id', '=', $category_id)->get();
    return json_encode($subcategories);

});
Route::get('/information/create/ajax-types', function () {
    $subcategory_id = $_GET['subcategory_id'];
    $types = \App\Models\Category::where('parent_id', '=', $subcategory_id)->get();
    return json_encode($types);

});
Route::get('/getchat', function () {
    $sender_id = $_GET['sender_id'];
    $receiver_id = $_GET['receiver_id'];
    $messages = \App\Models\Message::where([['sender_id', $sender_id], ['receiver_id', $receiver_id]])
        ->orWhere(function ($query) use ( $sender_id,$receiver_id) {
            return $query->where([['sender_id', $receiver_id], ['receiver_id', $sender_id]]);
        })->with('sender')->with('receiver')->get();
    return json_encode($messages);

});
Route::get('/sendmessage', function () {
    $sender_id = $_GET['sender_id'];
    $receiver_id = $_GET['receiver_id'];
    $message = $_GET['message'];
    $msg=\App\Models\Message::create([
        'sender_id'=>$sender_id,
        'receiver_id'=>$receiver_id,
        'message'=>$message,

    ]);
     return json_encode($msg);

});
Route::get('/change-country', function () {
    $country_id = $_GET['country_id'];
    session(['country_id' => $country_id]);

});
Route::get('/change-city', function () {
    $city_id = $_GET['city_id'];
    session(['city_id' => $city_id]);

});
