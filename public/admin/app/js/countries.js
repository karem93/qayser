$(document).ready(function () {
    $('#country_id').on('change', function (e) {
       
        var country_id = e.target.value;
        if (country_id) {
            $.ajax({
                url: '/information/create/ajax-countries?country_id=' + country_id,
                type: "GET",

                dataType: "json",

                success: function (data) {
                    $('#city_id').empty();

                    $('#city_id').append('<option value="">اختر المدينة</option>');
                    $.each(data, function (i, m) {
                        $('#city_id').append('<option value="' + m.id + '">' + m.name_ar + '</option>');
                    });
                }
            });
        } else {
            $('#city_id').empty();
        }
    });
    $('#header_country_id').on('change', function (e) {
        // console.log('enter');
        var country_id = e.target.value;
        if (country_id) {
            $.ajax({
                url: '/information/create/ajax-countries?country_id=' + country_id,
                type: "GET",

                dataType: "json",

                success: function (data) {
                    $('#header_city_id').empty();

                    $('#header_city_id').append('<option value="">اختر المدينة</option>');
                    $.each(data, function (i, m) {
                        $('#header_city_id').append('<option value="' + m.id + '">' + m.name_ar + '</option>');
                    });
                }
            });
        } else {
            $('#header_city_id').empty();
        }
    });
    $('.change_country').on('change', function (e) {
        var country_id = e.target.value;
        console.log(country_id)
        if (country_id) {
            $.ajax({
                url: '/change-country?country_id=' + country_id,
                type: "GET",

                dataType: "json",

                success: function (data) {

                }
            });
        }
    });
    $('.change_city').on('change', function (e) {
        var city_id = e.target.value;

            $.ajax({
                url: '/change-city?city_id=' + city_id,
                type: "GET",

                dataType: "json",

                success: function (data) {

                }
            });

    });
    $('#category_id').on('change', function (e) {
        var category_id = e.target.value;
        if (category_id) {
            $.ajax({
                url: '/information/create/ajax-categories?category_id=' + category_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('#subcategory_id').empty();
                    $('#subcategory_id').append('<option value="">اختر نوع الاعلان</option>');
                    $.each(data, function (i, m) {
                        $('#subcategory_id').append('<option value="' + m.id + '">' + m.name_ar + '</option>');
                    });
                }
            });
        } else {
            $('#subcategory_id').empty();
        }
    });
    $('#subcategory_id').on('change', function (e) {
        var subcategory_id = e.target.value;
        if (subcategory_id) {
            $.ajax({
                url: '/information/create/ajax-types?subcategory_id=' + subcategory_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('#type').empty();
                    $('#type').append('<option value="">اختر مواصفات الاعلان</option>');
                    $.each(data, function (i, m) {
                        $('#type').append('<option value="' + m.id + '">' + m.name_ar + '</option>');
                    });
                }
            });
        } else {
            $('#subcategory_id').empty();
        }
    });
    $('#searchFilter').click(function (e) {
        console.log('enter');

        var price_start = $("#price_start").val();
        var price_end = $("#price_end").val();

        var city_id = $("#city_id").val();
        var category_id = $("#category_id").val();
        var subcategory_id = $("#subcategory_id").val();
        var type = $("#type").val();

        var options = [];

        $.each($(".option_dev"), function(){
            var option = {id:$(this).find('input[type=hidden]').val(),value:$(this).find('select').val()};
            options.push(option);
        });
        var _token = $('#token').val();


        console.log(options);

        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '/search-filter',
            data: {
                price_start: price_start,
                price_end: price_end,
                city_id: city_id,
                category_id: category_id,
                subcategory_id: subcategory_id,
                options: JSON.stringify(options),
                type: type,
                _token: _token
            },
            processData: true,
            success: function (data) {
                console.log(data);


                $('#search_results').empty();
                var str = '';
                $.each(data, function (i, m) {
                    str += '<div class="col-md-4 col-xs-6 popular-item">' +
                        '<div class="thumb">' +
                        '<a href="/product/' + m.id + '">' +
                        '<img src="' + m.product_image[0].image + '" alt=""></a>' +
                        '<div class="text-content">' +
                        '<h4>' + m.title + '</h4>' +
                        '<span>' + m.price + ' ريال</span>' +
                        '</div>' +

                        '</div>' +
                        '</div>';
                });
                $('#search_results').append(str);

            }


        });

    });
    sendmsg();
    $('.chat_list').click(function (e) {
        $(".active_chat").removeClass("active_chat");
        $(this).addClass('active_chat');


        var sender_id = $(this).attr('sender-id');
        var receiver_id = $(this).attr('receiver-id');
        var user_id = $(this).attr('user-id');

        console.log(sender_id, receiver_id);

        $.ajax({
            url: '/getchat?sender_id=' + sender_id + '&receiver_id=' + receiver_id,
            type: "GET",

            dataType: "json",

            success: function (data) {
                console.log(data);
                $('#mesgs').empty();

                var str = '<div class="msg_history" id="msg_history">';
                $.each(data, function (i, m) {
                    if (m.sender.id != user_id) {
                        str += '<div class="incoming_msg">';
                        if (m.sender.photo) {
                            str += ' <div class="incoming_msg_img"> <img src="' + m.sender.photo + '" alt="sunil"> </div>';
                        } else {
                            str += ' <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>';


                        }
                        str += '<div class="received_msg">' +
                            '<div class="received_withd_msg">' +
                            '<p>' + m.message + '</p>' +
                            '<span class="time_date"> ' + m.created_at + '</span></div>' +
                            '</div>' +
                            '</div>';
                    } else {
                        str += '<div class="outgoing_msg">' +
                            '<div class="sent_msg">' +
                            '<p>' + m.message + '</p>' +
                            '<span class="time_date"> ' + m.created_at + '</span> </div>' +
                            '</div>';

                    }


                });
                str += '</div>' +
                    '<div class="type_msg">' +
                    '<div class="input_msg_write">' +
                    '<input type="text" class="write_msg" value="" placeholder="Type a message" />';
                if (data[0].sender_id == user_id) {

                    str += '<button class="msg_send_btn" sender-id="' + user_id + '" receiver-id="' + data[0].receiver_id + '" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>';
                } else {
                    str += '<button class="msg_send_btn" sender-id="' + user_id + '" receiver-id="' + data[0].sender_id + '" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>';

                }
                str += '</div>' +
                    '</div>';
                $('#mesgs').append(str);
                sendmsg();

            }


        });

    });

    function sendmsg() {
        $('.msg_send_btn').click(function (e) {

            var sender_id = $(this).attr('sender-id');
            var receiver_id = $(this).attr('receiver-id');
            var message = $(this).siblings('.write_msg').val();
            $(this).siblings('.write_msg').val('');
            var elem = $(this);


            console.log(sender_id, receiver_id, message);
            if (message) {


                $.ajax({
                    url: '/sendmessage?sender_id=' + sender_id + '&receiver_id=' + receiver_id + '&message=' + message,
                    type: "GET",

                    dataType: "json",

                    success: function (data) {


                        var str = '<div class="outgoing_msg">' +
                            '<div class="sent_msg">' +
                            '<p>' + data.message + '</p>' +
                            '<span class="time_date"> ' + data.created_at + '</span> </div>' +
                            '</div>';
                        elem.parent().parent().siblings('.msg_history').append(str);

                    }


                });
            }

        });

    }


    $('.phone_show_text').on('click', function (e) {
        $('.phone_show_text').css("display", "none");
        $('.phone_show_number').css("display", "block");
    });

});






