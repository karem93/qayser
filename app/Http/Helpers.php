<?php

header("Content-Type: text/html; charset=utf-8");

//use App\Models\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

//use FCM;

class Helpers
{

    public static function base_url()
    {
        return URL::to('/');
    }


    public static function the_image($one)
    {
        if ($one->image) {
            return url($one->image->path);
        } else {
            return 'https://via.placeholder.com/700x300.png';
        }
    }

    public static function the_image_sm($one)
    {
        if ($one->image) {
            return url($one->image->path);
        } else {
            return 'https://via.placeholder.com/70x70.png';
        }
    }

    public static function category_image($one)
    {
        if ($one) {
            return url($one);
        } else {
            return 'https://via.placeholder.com/70x70.png';
        }
    }

    public static function failFindId()
    {
        if (request()->header('Accept-Language') == 'ar') {
            $message = 'لا يوجد نتائج ';
        } else {
            $message = 'No results for this id';
        }
        return $message;
    }


    /**
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @param $unit
     * @return float|int
     */


    /**
     * @return string
     */
    public static function generateRandomString()
    {
        return Str::random(255);

    }

    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function CheckAuthorizedRequest()
    {
        return \request()->header('Access-Token');
    }


    /**
     * @param Request $request
     * @return array|string|null
     */
    public static function getLang()
    {
        return \request()->header('Accept-Language');
    }

    /**
     * @return mixed
     */
    public static function getLoggedUser()
    {
        $user = User:: where('tokens', Helpers::CheckAuthorizedRequest())->first();
        if ($user)
            return $user;
        else
            return 'No results';
    }

    /**
     * @param $user
     * @return bool
     */
    public static function updateFCMToken($user)
    {
        $fcm_token = \request()->header('fcm-token');
        $user->update([
            'device_token' => $fcm_token ? $fcm_token : null
        ]);
        return true;
    }


    /**
     * @param $token
     * @param $message
     * @return bool
     */
    public static function fcm_notification($token, $content, $title, $message)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'notification' => $message,
            'sound' => true,
            'title' => $title,
            'body' => $content,
            'priority' => 'high',
        ];

        $extraNotificationData = ["data" => $notification];

        $fcmNotification = [
            'to' => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AAAAzukcGb0:APA91bHM7z0BeRSFJM2VReTRqqr-XgfaSCy65SUz8u7B4SqnsJIEXUq3rxdw1RfQZj2kVA-cRGWgx0243pfq0NrLEYbL_jbQVHRAYkj45AD051Ddpou9uHUnLVptCrwxHfBn-LRusvKA',
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return true;
    }


    public static function send_sms($messageContent, $mobileNumber)
    {
        $user = 'Alqayser';
        $password = '123456';
        $sendername = 'ALQAYSER';
        $text = urlencode($messageContent);
        $to = $mobileNumber;
// auth call
        $url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=full";

//لارجاع القيمه json
//$url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=json";
// لارجاع القيمه xml
//$url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E&return=xml";
// لارجاع القيمه string
//$url = "http://www.oursms.net/api/sendsms.php?username=$user&password=$password&numbers=$to&message=$text&sender=$sendername&unicode=E";
// Call API and get return message
//fopen($url,"r");
        $ret = file_get_contents($url);
//        echo nl2br($ret);
        return $ret;
    }

}
