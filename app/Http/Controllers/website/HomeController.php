<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Slider;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    public function home()
    {
        $current_lang = App::getLocale();
        $categories = Category::where('parent_id', null)->get();
        $latest_products = Product::latest()->take(10);
        $most_viewed_products = Product::orderBy('views', 'DESC')->take(10);

        $city_id = session('city_id');
        if ($city_id) {
            $latest_products = $latest_products->where('city_id', $city_id);
            $most_viewed_products = $most_viewed_products->where('city_id', $city_id);


        }
        $latest_products = $latest_products->get();
        $most_viewed_products = $most_viewed_products->get();
        $setting = Setting::first();
        $setting->views = +1;
        $setting->save();

        $sliders = Slider::where('location', 1)->get();
        return view('welcome', compact('categories', 'current_lang', 'latest_products', 'most_viewed_products', 'sliders'));
    }
}
