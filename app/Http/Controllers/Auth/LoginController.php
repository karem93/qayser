<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $adminRedirectTo = '/webadmin/dashboard';
    protected $redirectTo = '/';


    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);
        if(is_numeric($request->username)){
            $loginby='phone';
        }else{
            $loginby='username';

        }

        if (Auth::attempt([$loginby=>$request->username,'password'=>$request->password]))
        {
            if (Auth::user()->active == 0){
                $this->guard()->logout();

                return redirect('/login')->with('error','هذا الحساب غير مفعل .');
            }else
                {
                if (Auth::user()->role == 1) {
                    return redirect()->intended($this->adminRedirectTo);
                } else {
                    return redirect()->intended($this->redirectTo);
                }
            }


        }
        else {

            return redirect('/login')->with('error','بيانات خاطئه .');
        }
    }
}
