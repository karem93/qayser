<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\CompleteProfileRequest;
use App\Http\Resources\UserProfileResource;
use App\Models\Fav;
use App\Models\Notification;
use App\Models\PasswordReset;
use App\Models\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class UserController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $oldemail = User::where('email', $request->email)->first();
        if ($oldemail) {
            return response()->json(['error' => 'Email Already Exist'], 400);
        }
        $oldeusername = User::where('username', $request->username)->first();
        if ($oldeusername) {
            return response()->json(['error' => 'username Already Exist'], 400);
        }
        $oldephone = User::where('phone', $request->phone)->first();
        if ($oldephone) {
            return response()->json(['error' => 'Phone Already Exist'], 400);
        }

        $user = User::create([
            'username' => $request->username,
            'phone' => $request->phone,
            'email' => $request->email,
            'country_id' => $request->country_id,
            'city_id' => $request->city_id,
            'password' => bcrypt($request->password),

        ]);

//        $code = rand(1111, 9999);
        $code = 1111;
        $user->update([
            'code' => $code,
            'code_expire_at' => Carbon::now()->addMinutes(5)
        ]);
        $user->update([
            'device_token' => $request->fcm_token
        ]);

        \Helpers::send_sms($code . 'كود التفعيل : ', $user->phone);

        return response()->json([
            'user_id' => $user->id,
            'code' => $user->code,
        ], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function activeUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $code = $request->code;
        $user_id = $request->user_id;

        $user = User::find($user_id);

        if (!$user) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        if ($user->code == $code) {
            if (Carbon::now() > $user->code_expire_at) {
                if (request()->header('Accept-Language') == 'ar')
                    $message = 'الكود منتهي .. برجاء الحصول على كود جديد';
                else
                    $message = 'expired  code, get new one';
                return response()->json(['error' => $message], 400);
            }
            $user->update([
                'active' => 1,
            ]);

            if ($user->tokens == null) {
                $access_token = \Helpers::generateRandomString();
                $user->update([
                    'tokens' => $access_token,
                    'active' => 1,
                ]);
            }
            return response()->json([
                'id' => $user->id,
                'username' => $user->username ? $user->username : '',
                'email' => $user->email,
                'phone' => $user->phone,
                'user_image' => $user->photo ? \Helpers::base_url() . '/' . $user->photo : '',
                'joined_from' => $user->created_at ? $user->created_at->toDateString() : '',
                'product_number' => 0,
                'myfav_number' => 0,
                'is_active' => $user->active == 0 ? false : true,
                'token' => $user->tokens,
                "country_id" => $user->country->id,
                "country" => App()->getLocale() == 'ar' ? $user->country->name_ar : $user->country->name_en,
                "city_id" => $user->city->id,
                "city" => App()->getLocale() == 'ar' ? $user->city->name_ar : $user->city->name_en,
            ], 200);

        } else {
            if (request()->header('Accept-Language') == 'ar')
                $message = 'الكود غير صالح';
            else
                $message = 'Invalid  code';
            return response()->json(['error' => $message], 400);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function resendUserActivationCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = User::find($request->user_id);

        if (!$user) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

//        $code = rand(1111, 9999);
        $code = 1111;
        $user->update([
            'code' => $code,
            'code_expire_at' => Carbon::now()->addMinutes(5)

        ]);

        \Helpers::send_sms($code . 'كود التفعيل : ', $user->phone);

        $user->save();

        return response()->json([
            'user_id' => $user->id,
            'login_code' => $code,
        ], 200);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        if (is_numeric($request->username)) {
            $loginby = 'phone';
        } else {
            $loginby = 'username';
        }

        if (Auth::attempt([$loginby => $request->username, 'password' => $request->password])) {
            $user = \auth()->user();

            if ($user->ban == 1) {
                \auth()->logout();
                return response()->json(['error' => 'Your Account Is Blocked'], 400);
            }

            $user->update([
                'device_token' => $request->fcm_token
            ]);
            $myproducts = Product::where('user_id', $user->id)->count();
            $myfavs = Fav::where('user_id', $user->id)->count();

            return response()->json([
                'id' => $user->id,
                'username' => $user->username ? $user->username : '',
                'email' => $user->email,
                'phone' => $user->phone,
                'user_image' => $user->photo ? \Helpers::base_url() . '/' . $user->photo : '',
                'joined_from' => $user->created_at ? $user->created_at->toDateString() : '',
                'product_number' => $myproducts,
                'myfav_number' => $myfavs,
                'country_id' => $user->country->id,
                'country' => App()->getLocale() == 'ar' ? $user->country->name_ar : $user->country->name_en,
                'city_id' => $user->city->id,
                'city' => App()->getLocale() == 'ar' ? $user->city->name_ar : $user->city->name_en,
                'is_active' => $user->active == 0 ? false : true,
                'token' => $user->tokens
            ], 200);
        }
        return response()->json(['error' => 'incorrect credentials'], 400);
    }

    public function getResetToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $user = User::where('phone', $request->phone)->first();
        if (!$user)
            return response()->json(['error' => 'phone Not Found'], 400);
        $old_reset_password = PasswordReset::where('phone', $request->phone)->first();
        $digits = 6;
        if (!$old_reset_password) {
            $code = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
            $reset_password = PasswordReset::create([
                'phone' => $request->phone,
                'token' => $code
            ]);

        } else {
            $code = $old_reset_password->token;
        }

        return response()->json(['code' => (int)$code], 200);

    }


    public function checkResetToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $reset_password = PasswordReset::where('phone', $request->phone)
            ->where('token', $request->code)->first();
        if (!$reset_password)
            return response()->json(['error' => 'invalid code'], 400);
        else
            return response()->json([], 204);

    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'new_password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = User::where('phone', $request->phone)->first();
        $reset_password = PasswordReset::where('phone', $request->phone)
            ->first();

        if ($reset_password == NULL || $user == null) {
            return response()->json(['error' => 'invalid data'], 400);
        } else {
            $user->password = bcrypt($request->new_password);
            $user->save();
            PasswordReset::destroy($reset_password->id);
            return response()->json([], 204);
        }

    }

    public function updatePassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        if (!Hash::check($request->old_password, $user->password)) {
            return response()->json(['error' => 'incorrect old password'], 400);
        } else {

            $user->password = bcrypt($request->new_password);
            $user->save();
            return response()->json([], 204);
        }
    }


    public function getProfile()
    {
        if (\Helpers::CheckAuthorizedRequest() == null)
            return response()->json(['error' => 'unauthorized'], 401);

        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }

        return response()->json(new UserProfileResource($user), 200);
    }


    public function updateProfile(Request $request)
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        if ($user->email != $request->email) {
            $old_email = User::where('email', $request->email)->exists();
            if ($old_email) {
                return response()->json(['error' => 'Email Already Exist'], 400);
            }
        }
        if ($user->username != $request->username) {
            $old_username = User::where('username', $request->username)->exists();
            if ($old_username) {
                return response()->json(['error' => 'username Already Exist'], 400);

            }
        }
        if ($user->phone != $request->phone) {
            $old_phone = User::where('phone', $request->phone)->exists();
            if ($old_phone) {
                return response()->json(['error' => 'phone Already Exist'], 400);

            }
        }
        $user->update([
            'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
            'city_id' => $request->city_id,
            'country_id' => $request->country_id,
        ]);
        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $imageName = Str::random(10) . '.' . $file->extension();
            $file->move(
                base_path() . '/public/images/users/', $imageName
            );
            $user->photo = '/images/users/' . $imageName;
            $user->save();
        }

        return response()->json([], 204);
    }

    /**
     * @return JsonResponse
     */
    public function getNotifications()
    {
        $user = \Helpers::getLoggedUser();
        $notifications = Notification::where('notifiable_id', $user->id)
            ->latest()
            ->select('id', 'data', 'read_at', 'created_at')
            ->get();
        foreach ($notifications as $notification) {
            $notification->data = json_decode($notification->data);
        }
        $user->unreadNotifications->markAsRead();
        return response()->json($notifications, 200);
    }

    /**
     * @return JsonResponse
     */
    public function notifiable()
    {
        $user = \Helpers::getLoggedUser();
        if (!$user || $user == 'No results') {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        if ($user->notifiable == 1) {
            $user->update(['notifiable' => 0]);
        } else {
            $user->update(['notifiable' => 1]);

        }
        return response()->json([
            'notifiable' => $user->notifiable
        ], 200);
    }

    /**
     * @return JsonResponse
     */
    public function deleteNotification()
    {
        $user = \Helpers::getLoggedUser();
        Notification::where('notifiable_id', $user->id)->delete();
        return response()->json([], 204);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function deleteOneNotification($id)
    {

        $note = Notification::where('id', $id)->first();
        if ($note) {
            $note->delete();
            return response()->json([], 204);
        } else {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
    }

    /**
     * @return JsonResponse
     */
    public function unreadNotifications()
    {
        $user = \Helpers::getLoggedUser();
        $count = $user->unreadNotifications->count();
        return response()->json(['count' => $count], 200);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateUserFCMToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'fcm_token' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $user->update([
            'device_token' => $request->fcm_token
        ]);
        return response()->json([], 204);
    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
        $user = \Helpers::getLoggedUser();
        if (!$user) {
            return response()->json(['error' => \Helpers::failFindId()], 400);
        }
        $user->update([
            'device_token' => null
        ]);
        return response()->json([], 204);
    }
}
