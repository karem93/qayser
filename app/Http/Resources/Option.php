<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Option extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' =>App()->getLocale()=='ar'?$this->name_ar:$this->name_en,
            'option' =>$this->Values
        ];
    }
}
