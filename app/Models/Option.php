<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'options';
    protected $guarded = [];

    public $timestamps = true;

    public function Values(){
        return $this->hasMany(OptionValue::class , 'option_id','id')->select('id','value as name');
    }
    public function Values2(){
        return $this->hasMany(OptionValue::class , 'option_id','id')->select('id','value as name','option_id');
    }
}
